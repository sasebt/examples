package com.sourcebt.atlassianassignment;

import android.util.Log;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleUnitTest {
    String source = "hello (there) @mate, how are you @ ? (smile) @me at http://sourcebt.com () http://www.google.com (ha ha ha) (1234567890123456)";
    String expectedResult = "{\"mentions\":[\"mate\",\"me\"],\"emotions\":[\"there\",\"smile\",\"ha ha ha\"],\"links\":[{\"title\":\"SourceBT - The Source of Brilliant Thinking\",\"url\":\"http://sourcebt.com\"},{\"title\":\"Google\",\"url\":\"http://www.google.com\"}]}";

    @Test
    public void testParseJsoup() throws Exception {
        String result = new AtlassianAssignmentParser().parse(source, false);
        System.out.println("testParseJsoup: " + result);
        assertEquals(result, expectedResult);
    }

    @Test
    public void testParseRegex() throws Exception {
        String result = new AtlassianAssignmentParser(false).parse(source, false);
        System.out.println("testParseRegex: " + result);
        assertEquals(result, expectedResult);
    }
}