package com.sourcebt.atlassianassignment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnParse)
    Button btnParse;
    @BindView(R.id.txtJson)
    TextView txtJson;
    @BindView(R.id.txtSource)
    EditText txtSource;
    @BindView(R.id.chkJsoup)
    CheckBox chkJsoup;

    ParseAsyncTask parseAsyncTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        btnParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parseText();
            }
        });
    }

    void parseText() {
        if (parseAsyncTask == null) {
            parseAsyncTask = new ParseAsyncTask(chkJsoup.isChecked(), txtSource.getText().toString());
            parseAsyncTask.execute();
        } else
            Snackbar.make(txtJson, "Please wait for the previous parsing operation to finish.", Snackbar.LENGTH_LONG).show();
    }

    class ParseAsyncTask extends AsyncTask<String, Integer, String> {
        EditText txtSource;
        boolean useJsoup;
        String source;
        long startTime;

        public ParseAsyncTask(boolean useJsoup, String source) {
            this.useJsoup = useJsoup;
            this.source = source;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            txtJson.setText("Parsing...");
            startTime=System.currentTimeMillis();
        }

        @Override
        protected String doInBackground(String... strings) {
            return new AtlassianAssignmentParser(useJsoup).parse(source, true);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            txtJson.setText(String.format("Parsed in %d ms:\n%s", System.currentTimeMillis()-startTime, s));
            parseAsyncTask = null;
        }
    }

}
