package com.sourcebt.atlassianassignment;

import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;

/**
 * This class implements a parser for a string (chat message) and produces a
 * json parsing out the emotions, mentions and URLs
 *
 * @author SStojanovski
 */
public class AtlassianAssignmentParser {

    private static final String REGEX_EMOTIONS = "\\(([^)]{1,15})\\)";
    private static final String REGEX_MENTIONS = "\\@(\\w+)";
    private static final String REGEX_URL = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    private static final String REGEX_TITLE = "<title>(.+)</title>";
    private boolean useJsoup = true;

    /**
     * Creates an instance of the parser for emotions mentions and links
     *
     * @param useJsoup - optional parameter whether the parser should use Jsoup library
     *                 to parse the title or use regex. Default is true
     */
    public AtlassianAssignmentParser(Boolean... useJsoup) {
        if (useJsoup != null && useJsoup.length > 0) {
            this.useJsoup = useJsoup[0];
        }
    }

    private List<String> getMatches(String source, String regex, int preCut, int postCut) {
        List<String> result = new ArrayList<>();
        Matcher matcher = Pattern.compile(regex).matcher(source);
        while (matcher.find()) {
            for (int i = 0; i < matcher.groupCount(); i++) {
                String match = matcher.group(i);
                result.add(match.substring(preCut, match.length() - postCut));
            }
        }
        return result;
    }

    private String getUrlTitle(String url) throws Exception {
        URLConnection urlc = new URL(url).openConnection();
        BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
        String line;
        String lines = "";
        while ((line = br.readLine()) != null) {
            lines += line;
            List<String> titles = getMatches(lines, REGEX_TITLE, 7, 8);
            if (titles.size() > 0) {
                br.close();
                return titles.get(0);
            }
        }
        br.close();
        return null;
    }

    private String getUrlTitleJsoup(String url) throws Exception {
        return Jsoup.connect(url).get().title();
    }

    /**
     * Parses a string into a JSON string containing emotions mentions and URLs.
     * <p>
     * Mentions are words that start with @, ex: @Joe, @Michael
     * <p>
     * Emotions are strings that are between parentheses, ex: (smile) (cry)
     * <p>
     * Urls are strings that start with http, ftp or file and follow the URL
     * pattern.
     * <p>
     * It connects to a web server to get the web page title for the specified
     * URL and is executed on the caller's thread. if there is a problem
     * retrieving the title, then null is used as a title
     *
     * @param source - the source string to be parsed
     * @param preety - whether the Json is formatted
     * @return - String with JSON object containing the emotions, mentions and
     * links
     */
    public String parse(String source, boolean preety) {
        JsonPojo j = new JsonPojo();
        j.emotions = getMatches(source, REGEX_EMOTIONS, 1, 1);
        j.mentions = getMatches(source, REGEX_MENTIONS, 1, 0);
        j.links = new ArrayList<>();

        List<String> urls = getMatches(source, REGEX_URL, 0, 0);
        for (String url : urls) {
            LinkPojo lp = new LinkPojo();
            try {
                if (useJsoup) {
                    lp.title = getUrlTitleJsoup(url);
                } else {
                    lp.title = getUrlTitle(url);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            lp.url = url;
            j.links.add(lp);
        }
        if (preety)
            return new GsonBuilder().setPrettyPrinting().create().toJson(j).toString();
        else
            return new GsonBuilder().create().toJson(j).toString();
    }
}
